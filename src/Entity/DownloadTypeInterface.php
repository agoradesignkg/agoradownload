<?php

namespace Drupal\agoradownload\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Entity\EntityDescriptionInterface;

/**
 * Defines the interface for download types.
 */
interface DownloadTypeInterface extends ConfigEntityInterface, EntityDescriptionInterface {

  const RESTRICTION_LEVEL_PRIVATE = 'private';

  const RESTRICTION_LEVEL_PUBLIC = 'public';

  /**
   * Gets the download type's restriction level.
   *
   * Gets the download type's restriction level, that will be used by access
   * control handlers. By default, we will support "public" and "private", but
   * it should be at least extendable by third party modules.
   *
   * @return string
   *   The restriction level.
   */
  public function getRestrictionLevel();

  /**
   * Sets the download type's restriction level.
   *
   * @param string $restriction_level
   *   The restriction level.
   *
   * @return $this
   */
  public function setRestrictionLevel($restriction_level);

}
