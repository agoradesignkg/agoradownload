<?php

namespace Drupal\agoradownload\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the download type entity class.
 *
 * @ConfigEntityType(
 *   id = "download_type",
 *   label = @Translation("Download type"),
 *   label_singular = @Translation("Download type"),
 *   label_plural = @Translation("Download types"),
 *   label_count = @PluralTranslation(
 *     singular = "@count download type",
 *     plural = "@count download types",
 *   ),
 *   handlers = {
 *     "form" = {
 *       "add" = "Drupal\agoradownload\Form\DownloadTypeForm",
 *       "edit" = "Drupal\agoradownload\Form\DownloadTypeForm",
 *       "delete" = "Drupal\agoradownload\Form\DownloadTypeDeleteForm"
 *     },
 *     "route_provider" = {
 *       "default" = "Drupal\Core\Entity\Routing\DefaultHtmlRouteProvider",
 *     },
 *     "list_builder" = "Drupal\agoradownload\DownloadTypeListBuilder",
 *   },
 *   config_prefix = "download_type",
 *   admin_permission = "administer download types",
 *   bundle_of = "download",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "description",
 *     "restriction_level",
 *   },
 *   links = {
 *     "add-form" = "/admin/structure/download-types/add",
 *     "edit-form" = "/admin/structure/download-types/{download_type}/edit",
 *     "delete-form" = "/admin/structure/download-types/{download_type}/delete",
 *     "collection" = "/admin/structure/download-types"
 *   }
 * )
 */
class DownloadType extends ConfigEntityBundleBase implements DownloadTypeInterface {

  /**
   * The download type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The download type label.
   *
   * @var string
   */
  protected $label;

  /**
   * The download type description.
   *
   * @var string
   */
  protected $description;

  /**
   * The download restriction level.
   *
   * @var string
   */
  protected $restriction_level;

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->description;
  }

  /**
   * {@inheritdoc}
   */
  public function setDescription($description) {
    $this->description = $description;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getRestrictionLevel() {
    return $this->restriction_level;
  }

  /**
   * {@inheritdoc}
   */
  public function setRestrictionLevel($restriction_level) {
    $this->restriction_level = $restriction_level;
    return $this;
  }

  /**
   * Returns available restriction levels.
   *
   * @return string[]
   *   The available restriction levels as string array. Keys and values are
   *   identical.
   */
  public static function getRestrictionLevels() {
    $levels = [
      DownloadTypeInterface::RESTRICTION_LEVEL_PRIVATE,
      DownloadTypeInterface::RESTRICTION_LEVEL_PUBLIC,
    ];
    return array_combine($levels, $levels);
  }

}
