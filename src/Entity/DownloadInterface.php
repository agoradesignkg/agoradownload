<?php

namespace Drupal\agoradownload\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\file\Entity\File;
use Drupal\taxonomy\TermInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Defines the interface for downloads.
 */
interface DownloadInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface, EntityPublishedInterface {

  /**
   * Gets the download title.
   *
   * @return string
   *   The download title
   */
  public function getTitle();

  /**
   * Sets the download title.
   *
   * @param string $title
   *   The download title.
   *
   * @return $this
   */
  public function setTitle($title);

  /**
   * Gets the category.
   *
   * @return \Drupal\taxonomy\TermInterface|null
   *   The category term, or null.
   */
  public function getCategory();

  /**
   * Sets the category.
   *
   * @param \Drupal\taxonomy\TermInterface $category
   *   The category term.
   *
   * @return $this
   */
  public function setCategory(TermInterface $category);

  /**
   * Gets the category ID.
   *
   * @return int
   *   The category ID.
   */
  public function getCategoryId();

  /**
   * Sets the category ID.
   *
   * @param int $category_id
   *   The category ID.
   *
   * @return $this
   */
  public function setCategoryId($category_id);

  /**
   * Gets the file.
   *
   * @return \Drupal\file\Entity\File
   *   The file entity.
   */
  public function getFile();

  /**
   * Sets the file.
   *
   * @param \Drupal\file\Entity\File $file
   *   The file entity.
   *
   * @return $this
   */
  public function setFile(File $file);

  /**
   * Gets the file ID.
   *
   * @return int
   *   The file ID.
   */
  public function getFileId();

  /**
   * Sets the file ID.
   *
   * @param int $file_id
   *   The file ID.
   *
   * @return $this
   */
  public function setFileId($file_id);

  /**
   * Gets whether the download is active.
   *
   * @return bool
   *   TRUE if the download is active, FALSE otherwise.
   */
  public function isActive();

  /**
   * Sets whether the download is active.
   *
   * @param bool $active
   *   Whether the download is active.
   *
   * @return $this
   */
  public function setActive($active);

  /**
   * Gets the download creation timestamp.
   *
   * @return int
   *   The download creation timestamp.
   */
  public function getCreatedTime();

  /**
   * Sets the download creation timestamp.
   *
   * @param int $timestamp
   *   The download creation timestamp.
   *
   * @return $this
   */
  public function setCreatedTime($timestamp);

}
