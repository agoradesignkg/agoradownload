<?php

namespace Drupal\agoradownload\Entity;

use Drupal\taxonomy\TermInterface;
use Drupal\user\UserInterface;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityPublishedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Url;
use Drupal\file\Entity\File;

/**
 * Defines the download entity class.
 *
 * @ContentEntityType(
 *   id = "download",
 *   label = @Translation("Download item"),
 *   label_singular = @Translation("Download item"),
 *   label_plural = @Translation("Download items"),
 *   label_count = @PluralTranslation(
 *     singular = "@count download item",
 *     plural = "@count download items",
 *   ),
 *   bundle_label = @Translation("Download type"),
 *   handlers = {
 *     "list_builder" = "Drupal\agoradownload\DownloadListBuilder",
 *     "storage" = "Drupal\agoradownload\DownloadStorage",
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "form" = {
 *       "default" = "Drupal\agoradownload\Form\DownloadForm",
 *       "add" = "Drupal\agoradownload\Form\DownloadForm",
 *       "edit" = "Drupal\agoradownload\Form\DownloadForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm"
 *     },
 *     "route_provider" = {
 *       "default" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider"
 *     }
 *   },
 *   admin_permission = "administer downloads",
 *   fieldable = TRUE,
 *   translatable = TRUE,
 *   base_table = "download",
 *   data_table = "download_field_data",
 *   entity_keys = {
 *     "id" = "download_id",
 *     "bundle" = "type",
 *     "label" = "title",
 *     "langcode" = "langcode",
 *     "uuid" = "uuid",
 *     "status" = "status",
 *     "published" = "status",
 *   },
 *   links = {
 *     "add-page" = "/agoradownload/add",
 *     "add-form" = "/agoradownload/add/{download_type}",
 *     "edit-form" = "/agoradownload/{download}/edit",
 *     "delete-form" = "/agoradownload/{download}/delete",
 *     "collection" = "/admin/content/downloads",
 *     "drupal:content-translation-overview" = "/agoradownload/{download}/translations",
 *     "drupal:content-translation-add" = "/agoradownload/{download}/translations/add/{source}/{target}",
 *     "drupal:content-translation-edit" = "/agoradownload/{download}/translations/edit/{language}",
 *     "drupal:content-translation-delete" = "/agoradownload/{download}/translations/delete/{language}",
 *   },
 *   bundle_entity_type = "download_type",
 *   field_ui_base_route = "entity.download_type.edit_form",
 * )
 */
class Download extends ContentEntityBase implements DownloadInterface {

  use EntityChangedTrait;
  use EntityPublishedTrait;

  /**
   * {@inheritdoc}
   */
  public function getTitle() {
    return $this->get('title')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setTitle($title) {
    $this->set('title', $title);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCategory() {
    return $this->get('category')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function setCategory(TermInterface $category) {
    $this->set('category', $category->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCategoryId() {
    return $this->get('category')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setCategoryId($category_id) {
    $this->set('category', $category_id);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getFile() {
    return $this->get('file')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function setFile(File $file) {
    $this->set('file', $file->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getFileId() {
    return $this->get('file')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setFileId($file_id) {
    $this->set('file', $file_id);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isActive() {
    return (bool) $this->getEntityKey('status');
  }

  /**
   * {@inheritdoc}
   */
  public function setActive($active) {
    $this->set('status', (bool) $active);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return (int) $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('uid')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('uid', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('uid')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    return $this->set('uid', $uid);
  }

  /**
   * {@inheritdoc}
   */
  public function toUrl($rel = 'canonical', array $options = []) {
    // StringFormatter assumes 'revision' is always a valid link template.
    if (in_array($rel, ['canonical', 'revision'])) {
      $file = $this->getFile();
      /** @var \Drupal\Core\File\FileUrlGeneratorInterface $file_url_generator */
      $file_url_generator = \Drupal::service('file_url_generator');
      $url_string = $file_url_generator->generateAbsoluteString($file->getFileUri());
      if (empty($options['attributes']['target'])) {
        $options['attributes']['target'] = '_blank';
      }
      if (empty($options['attributes']['rel'])) {
        $options['attributes']['rel'] = 'noopener';
      }
      return Url::fromUri($url_string, $options);
    }
    else {
      return parent::toUrl($rel, $options);
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);
    $fields += static::publishedBaseFieldDefinitions($entity_type);

    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Author'))
      ->setDescription(t('The download author.'))
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setDefaultValueCallback('Drupal\agoradownload\Entity\Download::getCurrentUserId')
      ->setTranslatable(TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 15,
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['title'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Title'))
      ->setDescription(t('The download title.'))
      ->setRequired(TRUE)
      ->setTranslatable(TRUE)
      ->setSetting('default_value', '')
      ->setSetting('max_length', 255)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['category'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Category'))
      ->setDescription(t('The category of this download.'))
      ->setSetting('target_type', 'taxonomy_term')
      ->setSetting('handler', 'default:taxonomy_term')
      ->setSetting('handler_settings', [
        'target_bundles' => ['download_categories' => 'download_categories'],
        'sort' => ['field' => '_none'],
        'auto_create' => TRUE,
        'auto_create_bundle' => 'download_categories',
      ])
      ->setDisplayOptions('form', [
        'type' => 'options_buttons',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['file'] = BaseFieldDefinition::create('file')
      ->setLabel(t('File'))
      ->setDescription(t('The download file.'))
      ->setRequired(TRUE)
      ->setTranslatable(TRUE)
      ->setSetting('file_directory', 'downloads')
      ->setSetting('file_extensions', agoradownload_allowed_file_extensions())
      ->setSetting('max_filesize', '')
      ->setSetting('description_field', FALSE)
      ->setSetting('handler', 'default:file')
      ->setSetting('display_field', FALSE)
      ->setSetting('display_default', FALSE)
      ->setSetting('uri_scheme', 'private')
      ->setSetting('target_type', 'file')
      ->setDisplayOptions('form', [
        'type' => 'file_generic',
        'weight' => 6,
        'settings' => [
          'progress_indicator' => 'throbber',
        ],
      ])
      ->setDisplayOptions('view', [
        'type' => 'file_default',
        'weight' => 1,
        'label' => 'hidden',
        'settings' => [],
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['status']
      ->setRevisionable(FALSE)
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => 99,
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time when the download was created.'))
      ->setTranslatable(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'datetime_timestamp',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time when the download was last edited.'))
      ->setTranslatable(TRUE);

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public static function bundleFieldDefinitions(EntityTypeInterface $entity_type, $bundle, array $base_field_definitions) {
    /** @var \Drupal\Core\Field\BaseFieldDefinition[] $fields */
    $fields = [];
    /** @var \Drupal\agoradownload\Entity\DownloadTypeInterface $download_type */
    $download_type = DownloadType::load($bundle);
    // $download_type could be NULL if the method is invoked during uninstall.
    if ($download_type && $download_type->getRestrictionLevel() == DownloadTypeInterface::RESTRICTION_LEVEL_PUBLIC) {
      // We override the file uri_scheme to public for public downloads.
      $fields['file'] = clone $base_field_definitions['file'];
      $fields['file']->setSetting('uri_scheme', 'public');
    }
    return $fields;
  }

  /**
   * Default value callback for 'uid' base field definition.
   *
   * @see ::baseFieldDefinitions()
   *
   * @return array
   *   An array of default values.
   */
  public static function getCurrentUserId() {
    return [\Drupal::currentUser()->id()];
  }

}
