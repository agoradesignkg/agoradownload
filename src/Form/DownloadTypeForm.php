<?php

namespace Drupal\agoradownload\Form;

use Drupal\agoradownload\Entity\DownloadType;
use Drupal\Core\Entity\BundleEntityFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\EntityTypeInterface;

/**
 * The download type form.
 */
class DownloadTypeForm extends BundleEntityFormBase {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);
    /** @var \Drupal\agoradownload\Entity\DownloadTypeInterface $download_type */
    $download_type = $this->entity;

    $form['#tree'] = TRUE;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $download_type->label(),
      '#description' => $this->t('Label for the download type.'),
      '#required' => TRUE,
    ];
    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $download_type->id(),
      '#machine_name' => [
        'exists' => '\Drupal\agoradownload\Entity\DownloadType::load',
        'source' => ['label'],
      ],
      '#maxlength' => EntityTypeInterface::BUNDLE_MAX_LENGTH,
    ];
    $form['restriction_level'] = [
      '#type' => 'select',
      '#title' => $this->t('Restriction level'),
      '#options' => DownloadType::getRestrictionLevels(),
      '#default_value' => $download_type->getRestrictionLevel(),
      '#description' => $this->t('Used by all downloads of this type.'),
    ];

    return $this->protectBundleIdElement($form);
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $this->entity->save();
    $this->messenger()->addStatus($this->t('Saved the %label download type.', ['%label' => $this->entity->label()]));
    $form_state->setRedirect('entity.download.collection');
  }

}
