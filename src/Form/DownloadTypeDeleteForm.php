<?php

namespace Drupal\agoradownload\Form;

use Drupal\Core\Entity\EntityDeleteForm;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Builds the form to delete an download type.
 */
class DownloadTypeDeleteForm extends EntityDeleteForm {

  /**
   * The donwload storage.
   *
   * @var \Drupal\agoradownload\DownloadStorageInterface
   */
  protected $downloadStorage;

  /**
   * Constructs a new DownloadTypeDeleteForm object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->downloadStorage = $entity_type_manager->getStorage('download');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $download_count = $this->downloadStorage->getQuery()
      ->condition('type', $this->entity->id())
      ->count()
      ->execute();
    if ($download_count) {
      $caption = '<p>' . $this->formatPlural($download_count, '%type is used by 1 download on your site. You can not remove this download type until you have removed all of the %type downloads.', '%type is used by @count downloads on your site. You may not remove %type until you have removed all of the %type downloads.', ['%type' => $this->entity->label()]) . '</p>';
      $form['#title'] = $this->getQuestion();
      $form['description'] = ['#markup' => $caption];
      return $form;
    }

    return parent::buildForm($form, $form_state);
  }

}
