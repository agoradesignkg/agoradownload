<?php

namespace Drupal\agoradownload\Plugin\Linkit\Matcher;

use Drupal\linkit\Plugin\Linkit\Matcher\EntityMatcher;

/**
 * Provides specific linkit matchers for download entities.
 *
 * @Matcher(
 *   id = "entity:download",
 *   label = @Translation("Download"),
 *   target_entity = "download",
 *   provider = "agoradownload"
 * )
 */
class DownloadMatcher extends EntityMatcher {

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies() {
    return parent::calculateDependencies() + [
      'module' => ['agoradownload'],
    ];
  }

}
