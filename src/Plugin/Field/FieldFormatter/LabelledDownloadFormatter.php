<?php

namespace Drupal\agoradownload\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\file\Plugin\Field\FieldFormatter\FileFormatterBase;

/**
 * Plugin implementation of the 'file_labelled_download' formatter.
 *
 * @FieldFormatter(
 *   id = "file_labelled_download",
 *   label = @Translation("Labelled download"),
 *   field_types = {
 *     "file"
 *   }
 * )
 */
class LabelledDownloadFormatter extends FileFormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    // @todo we could provide a config setting here instead of hardcoding.
    $label = 'Download';

    /** @var \Drupal\file\FileInterface $file */
    /* @noinspection PhpParamsInspection */
    foreach ($this->getEntitiesToView($items, $langcode) as $delta => $file) {
      /* @noinspection PhpUndefinedFieldInspection */
      $item = $file->_referringItem;

      $attributes = [];
      // Pass field item attributes to the theme function.
      if (isset($item->_attributes)) {
        $attributes = $item->_attributes;
        // Unset field item attributes since they have been included in the
        // formatter output and should not be rendered in the field template.
        unset($item->_attributes);
      }

      $icon = NULL;
      switch ($file->getMimeType()) {
        case 'application/pdf':
          $icon = 'file-pdf';
          break;
      }
      if ($icon) {
        $attributes['data-icon'] = $icon;
      }

      $elements[$delta] = [
        '#theme' => 'file_link',
        '#file' => $file,
        '#description' => $label,
        '#attributes' => $attributes,
        '#cache' => [
          'tags' => $file->getCacheTags(),
        ],
      ];
    }

    return $elements;
  }

}
