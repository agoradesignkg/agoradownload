<?php

namespace Drupal\agoradownload;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;

/**
 * Defines the list builder for download types.
 */
class DownloadTypeListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['label'] = $this->t('Download type');
    $header['id'] = $this->t('Machine name');
    $header['restriction_level'] = $this->t('Restriction level');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /** @var \Drupal\agoradownload\Entity\DownloadTypeInterface $entity */
    $row['label'] = $entity->label();
    $row['id'] = $entity->id();
    $row['restriction_level'] = $entity->getRestrictionLevel();
    return $row + parent::buildRow($entity);
  }

}
