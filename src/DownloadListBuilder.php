<?php

namespace Drupal\agoradownload;

use Drupal\agoradownload\Entity\DownloadType;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\taxonomy\TermStorageInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines the list builder for downloads.
 */
class DownloadListBuilder extends EntityListBuilder {

  /**
   * The date formatter.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * The taxonomy term storage.
   *
   * @var \Drupal\taxonomy\TermStorageInterface
   */
  protected $termStorage;

  /**
   * Holds the number of available categories.
   *
   * @var int|null
   */
  protected $categoryCount;

  /**
   * Constructs a new EntityListBuilder object.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type definition.
   * @param \Drupal\Core\Entity\EntityStorageInterface $storage
   *   The entity storage class.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter.
   * @param \Drupal\taxonomy\TermStorageInterface $term_storage
   *   The taxonomy term storage.
   */
  public function __construct(EntityTypeInterface $entity_type, EntityStorageInterface $storage, DateFormatterInterface $date_formatter, TermStorageInterface $term_storage) {
    parent::__construct($entity_type, $storage);

    $this->dateFormatter = $date_formatter;
    $this->termStorage = $term_storage;
    $this->categoryCount = NULL;
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static(
      $entity_type,
      $container->get('entity_type.manager')->getStorage($entity_type->id()),
      $container->get('date.formatter'),
      $container->get('entity_type.manager')->getStorage('taxonomy_term')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $category_count = $this->getCategoriesCount();
    $header['title'] = $this->t('Title');
    if ($category_count) {
      $header['category'] = $this->t('Category');
    }
    $header['type'] = $this->t('Type');
    $header['status'] = $this->t('Status');
    $header['owner'] = $this->t('Owner');
    $header['created'] = $this->t('Created');
    $header['changed'] = $this->t('Changed');
    $header['download_link'] = $this->t('Download link');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /** @var \Drupal\agoradownload\Entity\DownloadInterface $entity */

    $bundle = DownloadType::load($entity->bundle());
    $category_count = $this->getCategoriesCount();

    $row['title']['data'] = [
      '#type' => 'link',
      '#title' => $entity->label(),
      '#url' => $entity->toUrl('edit-form'),
    ];
    if ($category_count) {
      $row['category'] = $entity->getCategory() ? $entity->getCategory()
        ->label() : '';
    }
    $row['type'] = $bundle->label();
    $row['status'] = $entity->isActive() ? $this->t('Published') : $this->t('Not published');
    $row['owner'] = $entity->getOwner()->getDisplayName();
    $row['created'] = $entity->getCreatedTime() ? $this->dateFormatter->format($entity->getCreatedTime(), 'short') : '';
    $row['changed'] = $entity->getChangedTime() ? $this->dateFormatter->format($entity->getChangedTime(), 'short') : '';
    $link_options = [
      'absolute' => TRUE,
      'attributes' => [
        'target' => '_blank',
        'rel' => 'noopener',
      ],
    ];
    $row['download_link']['data'] = [
      '#type' => 'link',
      '#title' => $this->t('Download'),
      '#url' => $entity->toUrl('canonical', $link_options),
    ];

    return $row + parent::buildRow($entity);
  }

  /**
   * Get the categories count.
   *
   * @return int
   *   The number of categories.
   */
  protected function getCategoriesCount(): int {
    if (is_null($this->categoryCount)) {
      $count_query = $this->termStorage->getQuery()
        ->condition('vid', 'download_categories')
        ->accessCheck(FALSE)
        ->count();
      $this->categoryCount = (int) $count_query->execute();
    }
    return $this->categoryCount;
  }

}
