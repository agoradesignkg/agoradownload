<?php

namespace Drupal\agoradownload;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;

/**
 * The storage for download entities.
 *
 * Fires matching events for entity hooks.
 */
class DownloadStorage extends SqlContentEntityStorage implements DownloadStorageInterface {

}
