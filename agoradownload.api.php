<?php

/**
 * @file
 * Hooks provided by the agoradownload module.
 */

/**
 * Alter the allowed file extensions of download files.
 *
 * @param string[] &$file_extensions
 *   An array containing the social network ID as keys and the corresponding
 *   FontAwesome icon name (without "fa-" prefix) as values.
 */
function hook_agoradownload_allowed_file_extensions_alter(array &$file_extensions) {
  $file_extensions[] = 'xlsx';
}
